﻿#include <iostream>

void main()
{
    int limit;
    std::cout << "Enter your number ";
    std::cin>> limit;
    std::string isOdd;
    std::cout << "Entter odd or even with a lower case ";
    std::cin >> isOdd;
    for (int odd{ 1 }, even{ 0 }; odd <= limit && even <= limit; odd += 2, even += 2)
    {
        if (isOdd == "even")
        {
            std::cout << even << " ";
        }
        else if (isOdd == "odd")
        {
            std::cout << odd << " ";
        }
        else
        {
            std::cout << "Incorrect data, please try again ";
            break;
        }
    }

}
